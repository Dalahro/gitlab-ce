# frozen_string_literal: true

class TemplateFinder
  include Gitlab::Utils::StrongMemoize

  VENDORED_TEMPLATES = HashWithIndifferentAccess.new(
    dockerfiles: ::Gitlab::Template::DockerfileTemplate,
    gitignores: ::Gitlab::Template::GitignoreTemplate,
    gitlab_ci_ymls: ::Gitlab::Template::GitlabCiYmlTemplate,
    persos: ::Gitlab::Template::PersoTemplate
  ).freeze

  class << self
    def build(type, project, params = {})
      if type.to_s == 'licenses'
        LicenseTemplateFinder.new(project, params) # rubocop: disable CodeReuse/Finder
      else
        new(type, project, params)
      end
    end
  end

  attr_reader :type, :project, :params

  attr_reader :vendored_templates
  private :vendored_templates

  def initialize(type, project, params = {})
    @type = type
    @project = project
    @params = params

    @vendored_templates = VENDORED_TEMPLATES.fetch(type)
  end

  def execute
    if type.to_s == 'persos'
      vendored_templates.set_categories(list_categories(project))
    end

    if params[:name]
      vendored_templates.find(params[:name], project)
    else
      vendored_templates.all(project)
    end
  end

  def list_categories(project)
    repo = project.repository.tree(:head, ".gitlab/personalised_templates")

    if repo.nil?
      return {}
    end

    folders = project.repository.tree(:head, ".gitlab/personalised_templates").entries.map(&:path).select {|f| !f[1..-1].include?('.')}
    folders.map {|f| [f[31..-1], f[31..-1]]}.to_h
  end
end
