import FileTemplateSelector from '../file_template_selector';

export default class PersonalisedSelector extends FileTemplateSelector {
  constructor({ mediator }) {
    super(mediator);
    this.config = {
      key: 'personalised',
      name: 'Personalised template',
      pattern: /(Personalised)/,
      type: 'personalised',
      dropdown: '.js-perso-selector',
      wrapper: '.js-perso-selector-wrap',
    };
  }

  initDropdown() {
    // maybe move to super class as well
    this.$dropdown.glDropdown({
      data: this.$dropdown.data('data'),
      filterable: true,
      selectable: true,
      toggleLabel: item => item.name,
      search: {
        fields: ['name'],
      },
      clicked: options => this.reportSelectionName(options),
  
      text: item => item.name,
    });
  }
}
