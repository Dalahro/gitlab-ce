# frozen_string_literal: true

module Gitlab
  module Template
    class PersoTemplate < BaseTemplate
      @categories = {}
      def name
        # File.basename(@path, self.class.extension)
        File.basename(@path)
      end

      class << self
        def extension
          '.*'
        end

        def categories
          @categories
        end

        def set_categories(categories)
          @categories = categories
        end

        def base_dir
          '.gitlab/personalised_templates'
        end

        def finder(project)
          Gitlab::Template::Finders::PersoTemplateFinder.new(project, self.base_dir, self.extension, self.categories)
        end
      end
    end
  end
end
